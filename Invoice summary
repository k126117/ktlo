/*********************************************************************************************
Invoice summary Overall hist 2018-2021 code
Description: Code for Invoice summary and RSM view
**********************************************************************************************/

DROP table if exists sand_comops.invoice_summary_2021_hist;
CREATE TABLE sand_comops.invoice_summary_2021_hist stored AS parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_comops.db/invoice_summary_2021_hist/' AS
SELECT t1.*
from(
select 
    pa.party_name,
    ca.ACCOUNT_NUMBER,
    cb.account_number as bill_to_number,
    pb.party_name as bill_to_name,    
    sum(k.REVENUE_AMOUNT) as REVENUE_AMOUNT,
    sum(k.THERAPY_DAYS) as THERAPY_DAYS,
    pd.PRODUCT_ITEM_DESCRIPTION,
    k.RENTAL_SALE_FLAG,
    k.MARKET_SEGMENT,
    k.TRX_TYPE,
    sum(k.QUANTITY_SOLD) as QUANTITY_SOLD,
    k.uom_code,
    k.START_DATE,
    k.END_DATE,
    k.ORDER_NUMBER,
    k.GL_DATE,
    k.ITEM_NUMBER,
    k.ORIG_ORDER_NUM,
    k.TRX_NUMBER,
    k.TRX_DATE,
    k.PATIENT_FIRST_NAME,
    k.PATIENT_LAST_NAME,
    ol.ATTRIBUTE8 as PATIENT_LOCATION,
    k.PO_NUMBER,
    k.REASON_CODE,
    k.SERIAL_NUMBER,
    st.REGION,
    st.DISTRICT,
    st.TERRITORY_CODE,
    st.salesrep_name,
    st.dm_name,
    pd.PRODUCT_BRAND,
    pd.global_sales_code,
    k.gl_date_par,
    case when market_segment in ('01','02','04','05','06','17','20' ,'40','89') and substr(st.territory_code,1,3) in ('WHM','WHM_HYBRID') then 'Y'
    when market_segment in ('14','15','16','19','33','34','50', '60','72','52','18','21','22','39','70','71','38','53','99') 
    and substr(st.territory_code,1,3) in ('AWT') then 'Y' else 'N' end as role_flag
from ebs_usawt.apps_kar_revenue k

join aim.aim_product_dim pd
on pd.product_sku = k.item_number
and pd.inventory_item_id = k.inventory_item_id
and k.gl_date_par >= 201801
and k.gl_date_par <= 202112
and DECODE( k.REV_TYPE,'FRT','BILLED FREIGHT',decode( k.THERAPY_CATEGORY,NULL,'No Therapy Category Assigned', 'ROTOPRONE','CRITICAL CARE', k.THERAPY_CATEGORY))  =  'VAC'
and k.set_of_books_id = 1

left join ebs_usawt.apps_oe_order_lines_all ol
on k.header_id = ol.header_id 
and k.line_id = ol.line_id 
and ol.creation_date >= '2018-01-01 00:00:00' 
and ol.creation_date <= '2021-12-31 00:00:00'

join ebs_usawt.apps_hz_cust_accounts ca
on ca.cust_account_id = k.ship_to_customer_id 

join ebs_usawt.apps_hz_cust_accounts cb
on cb.cust_account_id = k.bill_to_customer_id

join ebs_usawt.apps_hz_parties pa 
on pa.party_id = ca.party_id

and pa.party_type not in( 'PARTY_RELATIONSHIP','PERSON')

join ebs_usawt.apps_hz_parties pb 
on pb.party_id = cb.party_id

and pa.party_type not in( 'PARTY_RELATIONSHIP','PERSON')

join (select distinct s.customer_shipto_account_id
,s.customer_shipto_site_use_id
,s.customer_shipto_account_number
,t.territory_code 
,t.salesrep_name
,t.district
,t.dm_name
,t.region
from aim.aim_customer_shipto_dim s
inner join reporting_comops.aim_customer_zipterr_xref_transposed_vw  t on t.customer_key=s.customer_shipto_key 
and t.geography in ('EAST','WEST')
	) st
on k.SHIP_TO_SITE_USE_ID =st.customer_shipto_site_use_id
and ca.account_number = st.customer_shipto_account_number

where (
	 st.territory_code like "AWT%"
    or st.territory_code like "WHM%") 
and k.MARKET_SEGMENT <> '22'
     
group by 1,2,3,4,7,8,9,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35)t1
where t1.role_flag='Y';

compute stats   sand_comops.invoice_summary_2021_hist; 


/*********************************************************************************************
Invoice summary Overall Prod code
Description: Code for National and RSM view including historical data
*********************************************************************************************/

DROP table if exists sand_comops.invoice_summary;
CREATE TABLE sand_comops.invoice_summary stored AS parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_comops.db/invoice_summary/' AS
select t1.*
from(
select 
    pa.party_name,
    ca.ACCOUNT_NUMBER,
    cb.account_number as bill_to_number,
    pb.party_name as bill_to_name,    
    sum(k.REVENUE_AMOUNT) as REVENUE_AMOUNT,
    sum(k.THERAPY_DAYS) as THERAPY_DAYS,
    pd.PRODUCT_ITEM_DESCRIPTION,
    k.RENTAL_SALE_FLAG,
    k.MARKET_SEGMENT,
    k.TRX_TYPE,
    sum(k.QUANTITY_SOLD) as QUANTITY_SOLD,
    k.uom_code,
    k.START_DATE,
    k.END_DATE,
    k.ORDER_NUMBER,
    k.GL_DATE,
    k.ITEM_NUMBER,
    k.ORIG_ORDER_NUM,
    k.TRX_NUMBER,
    k.TRX_DATE,
    k.PATIENT_FIRST_NAME,
    k.PATIENT_LAST_NAME,
    ol.ATTRIBUTE8 as PATIENT_LOCATION,
    k.PO_NUMBER,
    k.REASON_CODE,
    k.SERIAL_NUMBER,
    st.REGION,
    st.DISTRICT,
    st.TERRITORY_CODE,
    st.salesrep_name,
    st.dm_name,
    pd.global_sales_code,
    pd.PRODUCT_BRAND,
    k.gl_date_par,
   	case when market_segment in ('01','02','04','05','06','17','20','40','89') and substr(st.territory_code,1,3) in ('WHM','WHM_HYBRID') then 'Y'
    when market_segment in ('14','15','16','19','33','34','50','60','72','52','18','21','22','39','70','71','38','53','99') 
    and substr(st.territory_code,1,3) in ('AWT') then 'Y' else 'N' end as role_flag

from ebs_usawt.apps_kar_revenue k

join aim.aim_product_dim pd
on pd.product_sku = k.item_number
and pd.inventory_item_id = k.inventory_item_id
and k.gl_date_par >= 202201
and DECODE( k.REV_TYPE,'FRT','BILLED FREIGHT',decode( k.THERAPY_CATEGORY,NULL,'No Therapy Category Assigned', 'ROTOPRONE','CRITICAL CARE', k.THERAPY_CATEGORY))  =  'VAC'
and k.set_of_books_id = 1

left join ebs_usawt.apps_oe_order_lines_all ol
on k.header_id = ol.header_id 
and k.line_id = ol.line_id 
and ol.creation_date >= '2022-01-01 00:00:00'


join ebs_usawt.apps_hz_cust_accounts ca
on ca.cust_account_id = k.ship_to_customer_id 

join ebs_usawt.apps_hz_cust_accounts cb
on cb.cust_account_id = k.bill_to_customer_id

join ebs_usawt.apps_hz_parties pa 
on pa.party_id = ca.party_id
and pa.party_type not in( 'PARTY_RELATIONSHIP','PERSON')

join ebs_usawt.apps_hz_parties pb 
on pb.party_id = cb.party_id
and pb.party_type not in( 'PARTY_RELATIONSHIP','PERSON')

join (select distinct s.customer_shipto_account_id
,s.customer_shipto_site_use_id
,s.customer_shipto_account_number
,t.territory_code 
,t.salesrep_name
,t.district
,t.dm_name
,t.region
from aim.aim_customer_shipto_dim s
inner join reporting_comops.aim_customer_zipterr_xref_transposed_vw  t on t.customer_key=s.customer_shipto_key 
and t.geography in ('EAST','WEST')
	) st
on k.SHIP_TO_SITE_USE_ID =st.customer_shipto_site_use_id
and ca.account_number = st.customer_shipto_account_number

where (
	 st.territory_code like "AWT%"
   or st.territory_code like "WHM%")
and k.MARKET_SEGMENT <> '22'
and k.trx_type in ('INVOICE','PENDING INVOICE','CREDIT MEMO')
and st.TERRITORY_CODE not like '%999%'
group by 1,2,3,4,7,8,9,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35 )t1
where t1.role_flag='Y'
    
union all 

select * from 	sand_comops.invoice_summary_2021_hist t2
where t2.territory_code not like '%999%'
and t2.trx_type in ('INVOICE','PENDING INVOICE','CREDIT MEMO')
;

compute stats   sand_comops.invoice_summary; 



